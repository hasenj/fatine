﻿var pageLoadTime = Date.now();

function getElementId(e) {
    return e.getAttribute("id");
}

function getSectionElement(section) {
    return document.getElementById(section);
}

function getFirstSectionElement() {
    return document.querySelector("section");
};

function showElement(e) {
    if(!e) {
        console.error("Invalid Element");
        return;
    }
    if(e.classList.contains("visible")) {
        return; // nothing to do
    }
    e.classList.add("visible");

    // make sure we don't alter scroll position
    var scrollPos = getScrollPosition();
    if(e.offsetTop < scrollPos) {
        setScrollPosition(scrollPos + e.offsetHeight);
    }
}

function detectCurrentSectionElement() {
    var elements = document.querySelectorAll("section.visible");
    var scrollPos = getScrollPosition();
    for(var i = 0; i < elements.length; i++) {
        var e = elements[i];
        var offset = document.documentElement.clientHeight * 0.4;
        if(isWithinElement(scrollPos + offset, e)) {
            return e;
        }
    }
    return null;
}

function gotoSection(section, relativeOffset) {
    relativeOffset = relativeOffset || 0;
    var element = getSectionElement(section);
    showSectionByElement(element);

    // defer ..
    setTimeout(function() {
        var position = element.offsetTop + relativeOffset;
        setScrollPosition(position);
    });
}

function showSectionByElement(e) {
    showElement(e);

    // show X next elements
    var n = e;
    for(var i = 0; i < 5; i++) {
        n = nextSectionElement(n);
        if(!n) {
            break;
        }
        showElement(n);
    }

    // show X previous elements
    var p = e;
    for(var i = 0; i < 5; i++) {
        p = previousSectionElement(p);
        if(!p) {
            break;
        }
        showElement(p);
    }
}

function setScrollPosition(position) {
    window.scrollTo(0, position);
    // console.debug("Scrolling to:", position)
}

/**
    Smoothly scroll element to the given target (element.scrollTop) for the given duration

    Returns a promise that's fulfilled when done
 */
function smoothScroll(element, target, duration) {
    target = Math.round(target);
    duration = Math.round(duration);
    if (duration < 0) {
        return Promise.reject("bad duration");
    }
    if (duration === 0) {
        element.scrollTop = target;
        return Promise.resolve();
    }

    var startTime = Date.now();
    var endTime = startTime + duration;

    var startTop = element.scrollTop;
    var difference = target - startTop;

    // based on http://en.wikipedia.org/wiki/Smoothstep
    var smoothStep = function(start, end, point) {
        if(point <= start) { return 0; }
        if(point >= end) { return 1; }
        var x = (point - start) / (end - start); // interpolation
        return x*x*(3 - 2*x);
        // return x*x*x*(x*(x*6 - 15) + 10);
    }

    return new Promise(function(resolve, reject) {
        var previousTop = element.scrollTop;
        var scrollFrame = function() {
            /*
            if(Math.abs(element.scrollTop - previousTop) > 1) {
                // console.log("animation interrupted; aborting");
                reject("interrupted");
                return;
            }
            */
            var now = Date.now();
            var point = smoothStep(startTime, endTime, now); // point is clamped to [0-1]
            var frameTarget = Math.round(startTop + (difference * point));
            element.scrollTop = frameTarget;

            if(now >= endTime) {
                resolve();
                return;
            }
            previousTop = element.scrollTop;
            setTimeout(scrollFrame, 0); // tick
        }
        setTimeout(scrollFrame, 0);
    });
}

var noChapterScroll = false;
function ensureChapterLinkVisible(noTransition) {
    if(noChapterScroll) {
        return;
    }
    var sidebar = document.querySelector("aside.contents");
    var chapterLink = sidebar.querySelector("li.active");
    if(!chapterLink) {
        return;
    }

    var sidebarViewportBottom = sidebar.scrollTop + sidebar.clientHeight;
    var chapterLinkBottom = chapterLink.offsetTop + chapterLink.scrollHeight;

    var sidebarViewportTop = sidebar.scrollTop;
    var chapterLinkTop = chapterLink.offsetTop;

    if ((sidebarViewportBottom < chapterLinkBottom) ||
        (sidebarViewportTop > chapterLinkTop)) {
        var middle = Math.ceil(sidebar.clientHeight * 0.4);
        var target = chapterLinkTop - middle;
        var duration = 200;
        if(noTransition) {
            duration = 1;
        }
        smoothScroll(sidebar, target, duration);
    }
}

function restoreLastPlace() {
    var lastSection = localStorage.getItem("lastSection");
    var lastScrollPosition = localStorage.getItem("lastScrollPosition");

    if(lastSection) {
        lastScrollPosition = Number(lastScrollPosition);
        gotoSection(lastSection, lastScrollPosition);
    } else {
        var firstEl = getFirstSectionElement();
        showSectionByElement(firstEl);
    }
}

function getScrollPosition() {
    return document.documentElement.scrollTop || document.body.scrollTop;
}

function sibling(element, direction) {
    var prop = "nextElementSibling";
    if(direction < 0) {
        prop = "previousElementSibling";
    }
    var nodeName = element.nodeName;
    while(element[prop]) {
        element = element[prop];
        if(element.nodeName === nodeName) {
            return element;
        }
    }
    return null;
}

function nextSectionElement(e) {
    return sibling(e, 1);
}

function previousSectionElement(e) {
    return sibling(e, -1);
}

function isWithinElement(scrollPosition, element) {
    var sectionTop = element.offsetTop;
    var sectionBottom = sectionTop + element.offsetHeight;
    return scrollPosition >= sectionTop && scrollPosition < sectionBottom;
}

function onScroll(event) {
    // console.log("Scrolling", getScrollPosition());
    var scrollPos = getScrollPosition();
    var e = detectCurrentSectionElement();
    if(e) {
        // console.debug("In section:", getElementId(e));
        showSectionByElement(e);
        var section = getElementId(e);
        var relativePos = scrollPos - e.offsetTop;
        // console.debug("Relative position:", relativePos.toFixed(2));
        localStorage.setItem("lastSection", section);
        localStorage.setItem("lastScrollPosition", relativePos.toFixed(2));
        sideBarSetActive(section);
    } else {
        // console.debug("Not in a section!!");
        // XXX what if it's because we've reached the end?
        localStorage.removeItem("lastSection");
        localStorage.removeItem("lastScrollPosition");
        sideBarSetActive("!");
    }
}

function sideBarSetActive(section) {
    var sideSections = document.querySelectorAll("aside.contents li");
    var matchingHref = "#" + section;

    var currentActive = document.querySelector("aside.contents li.active a");
    if(currentActive && currentActive.getAttribute("href") === matchingHref) {
        return; // nothing to do
    }

    for(var i = 0; i < sideSections.length; i++) {
        var sideSection = sideSections[i];
        var link = sideSection.querySelector("a");
        if(link.getAttribute("href") === matchingHref) {
            sideSection.classList.add("active");
        } else {
            sideSection.classList.remove("active");
        }
    }

    var now = Date.now();
    var noTransition = (now - pageLoadTime) < 3000; // if just loaded ..
    ensureChapterLinkVisible(noTransition);
}

function main() {
    console.debug("main()");
    try {
        restoreLastPlace();
    } catch(e) {
        // let it pass
        console.warn("couldn't restore last navigation place");
    }

    // start tracking scroll events ..
    document.addEventListener("scroll", onScroll);

    // hijack sidebar clicks
    var sidebar = document.querySelector("aside.contents");
    sidebar.addEventListener("click", function(e) {
        var node = e.target;
        if(node.nodeName.toLowerCase() === "a") {
            e.preventDefault();
            var href = node.getAttribute("href");
            var section = href.substring(1);
            gotoSection(section);
            sideBarSetActive(section); // XXX manual state update
            mobileHideIndex(); // HACK
        }
    });

    // Prevent scrolling over the chapters area from scrolling the content area
    sidebar.addEventListener("mouseover", function(e) {
        noChapterScroll = true; // XXX mutating global state
    });
    sidebar.addEventListener("mouseout", function(e) {
        noChapterScroll = false; // XXX mutating global state
    });

    // HACK
    var contentdiv = document.querySelector("div#content");
    contentdiv.addEventListener("click", function() {
        mobileHideIndex(); // HACK
    });
}

function mobileToggleIndex() {
    var sidebar = document.querySelector("aside.contents");
    sidebar.classList.toggle("mobile_visible");

    // if we just enabled it, make sure current chapter is visible
    if(sidebar.classList.contains("mobile_visible")) {
        ensureChapterLinkVisible();
    }
}

function mobileHideIndex() {
    var sidebar = document.querySelector("aside.contents");
    if(sidebar.classList.contains("mobile_visible")) { // useless check ..
        sidebar.classList.remove("mobile_visible");
    }
}

window.onload = main;
